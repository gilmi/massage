
{-# LANGUAGE GeneralizedNewtypeDeriving, TemplateHaskell #-}

-- | Types for the system
module Types
  where


import qualified Data.Text      as T
import qualified Data.Text.Lazy as TL
import qualified Data.ByteString as BS
import qualified Data.Thyme.Clock as Time

import Data.Label (mkLabels)

type Name = T.Text
type Mail = T.Text

data Pass = Pass
  { _pSalt :: BS.ByteString
  , _pHash :: BS.ByteString
  }
  deriving (Show, Eq, Ord) -- , NFData)

newtype Timestamp = Timestamp Time.UTCTime
  deriving (Eq, Ord) -- , NFData)

instance Show Timestamp where
  show _ = show "<time>"

-- | The model of the system are Users and Rooms:

{- | Users
   * Users are the endpoints wanting to communicate with one another.
   * Users interact with the system, sending messages to different rooms.
   * A user is a nickname, email for validation and a password (maybe also avatar? links? profile?)
   * A user has the list of messages they still need to receive and those they have sent but have yet been received
   * A user has a maybe connection in and connection out, to send and receive messages when they are online

-}

data User = User
  { _uName :: Name
  , _uMail :: Mail
  , _uPass :: Pass
  }
  deriving (Show, Eq, Ord) -- , NFData)

{- | Rooms
   * Rooms are places where users can chat.
   * Rooms have a name and a title, they also store the conversation log and who is in the room and what their permissions are.
   * A user permission for a room can either be Normal or Admin. A Normal user can send and receive messages written in the room,
     An Admin can invite or accept new users to the room, change their permissions or change the room title.

There are a few default rooms to convey messages to the user, such as "Errors" room.
-}

data Room = Room
  { _rName  :: Name
  , _rTitle :: T.Text
  , _rList  :: [(User, Permission)]
  , _rLog   :: [LogMsg]
  }
  deriving (Show, Eq, Ord) -- , NFData)

data Permission
  = Normal
  | Admin
  deriving (Show, Read, Eq, Ord, Enum) -- , NFData)

data LogMsg = LogMsg
  { _lText :: TL.Text
  , _lUser :: User
  , _lTime :: Timestamp
  }
  deriving (Show, Eq, Ord) -- , NFData)

data Message = Message
  { _mText :: TL.Text
  , _mUser :: User
  , _mTime :: Timestamp
  , _mRoom :: Name
  }
  deriving (Show, Eq, Ord) -- , NFData)

mkLabels [''Message, ''User, ''Room, ''LogMsg]


msgToLogMsg :: Message -> LogMsg
msgToLogMsg Message{..} = LogMsg _mText _mUser _mTime

{- | Components
This system will be composed from a few components:

* Users - Inter communication with users
* Router/Muxer - Interpreting messages from users and directs them to the right sub-system and back
* Login - Interacts with connection initiation
* Rooms - Adds messages to the chat and sends them back to relevant users, changes user status on rooms


- Each User has two threads for communication (in/out) and two more to communicate with an actual user when possible
- each connection for the login has a thread

-}
