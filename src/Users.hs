{-# LANGUAGE TemplateHaskell, TypeOperators, OverloadedStrings #-}

{- | Users communication

This module handles the communication with users and managing
messages from and to them

-}

module Users where


import Data.Monoid ((<>))
import qualified Data.Text.IO as T
import qualified Data.Text as T

import qualified Control.Monad.STM as STM
import qualified Control.Concurrent.STM as STM
import qualified Data.Map as M

import Types
import Utils
import qualified Handler as H

import Data.Label as Lens

{- | Design

We'll start with an in memory chat for starters.

For that we'll need:

* A data structure to hold the users and the connection to them
* A way to add users to the DS
* A way to send a message to users
* A way to open communication to users
* A way to store messages to and from users

-}

data MsgIn
  = SendMessage Message
  | AddUser User
  | RemUser User
  | CloseSocks User
  | OpenSocks User Socks

data MsgOut
  = GotMessage Message
  | LogOut User
  deriving Show


data ConnErr
  = UserNotFound User
  | UserExists User (Conn Message)
  | NoSocks

printErr :: ConnErr -> T.Text
printErr = \case
  UserNotFound u -> "User '" <> _uName u <> "' not found."
  UserExists u _ -> "User '" <> _uName u <> "' already exists."
  NoSocks -> "User is not connected."

type Users = M.Map User (Conn Message)

addUser :: User -> Users -> Either (User, Conn Message) Users
addUser user users =
  case M.lookup user users of
    Just cn -> throwErr (user, cn)
    Nothing -> pure $ M.insert user defaultConn users


remUser :: User -> Users -> Either ConnErr Users
remUser user users =
  case M.lookup user users of
    Nothing -> throwErr (UserNotFound user)
    Just _  -> pure $ M.delete user users


-- | Returns old socks to be closed
openSocks :: Socks -> User -> Users -> (Users, Maybe Socks)
openSocks s u us = case addUser u us of
  Right users -> openSocks s u users
  Left (_, c) -> (M.insert u (Lens.set conn (Just s) c) us, Lens.get conn c)


-- | Returns old socks to be closed
closeSocks :: User -> Users -> Either ConnErr (Users, Socks)
closeSocks u us = case M.lookup u us of
  Nothing -> throwErr (UserNotFound u)
  Just c ->
    maybe
      (throwErr NoSocks)
      (pure . (M.insert u (Lens.set conn Nothing c) us,))
      (Lens.get conn c)

sendMessage :: Message -> Users -> Either ConnErr Users
sendMessage m@(Lens.get mUser -> u) us = case M.lookup u us of
  Nothing -> throwErr (UserNotFound u)
  Just cn ->
    pure . (\c -> M.insert u c us) $ Lens.modify uQueueIn (push m) cn


{- | UsersHandler
     uses for communication in and out of the users handler
-}


type UsersHandler = H.Handler MsgIn

new :: STM.TVar Users -> STM.TQueue MsgOut -> IO UsersHandler
new statevar = H.newHandler statevar inHandler outHandler


{- | inHandler

resposible on getting messages from inside the application and handle them
-}

inHandler :: STM.TVar Users -> STM.TQueue MsgIn -> IO [MsgOut]
inHandler us input = do
  action <- STM.atomically $ do
    action <- STM.readTQueue input
    case action of
      SendMessage msg ->
        sendMessageSTM us msg

      AddUser user ->
        addUserSTM us user
  
      RemUser user ->
        remUserSTM us user
  
      CloseSocks user ->
        closeSocksSTM us user

      OpenSocks user socks ->
        openSocksSTM us user socks
  action

sendMessageSTM :: STM.TVar Users -> Message -> STM.STM (IO [MsgOut])
sendMessageSTM us msg = do
  users <- STM.readTVar us
  case sendMessage msg users of
    Right users' -> do
      STM.writeTVar us users'
      pure $ T.putStrLn ("Message Sent.\n  " <> T.pack (show msg)) >> pure []
    Left err -> do
      pure $ T.putStrLn ("Error when sending message: " <> printErr err) >> pure []

addUserSTM :: STM.TVar Users -> User -> STM.STM (IO [MsgOut])
addUserSTM us user = do
  users <- STM.readTVar us
  case addUser user users of
    Right users' -> do
      STM.writeTVar us users'
      pure $ T.putStrLn ("User '" <> _uName user <> "' added.\n  " <> T.pack (show $ M.keys users')) >> pure []
    Left (u, c) -> do
      pure $ T.putStrLn ("Error when adding new user: " <> printErr (UserExists u c)) >> pure []

remUserSTM :: STM.TVar Users -> User -> STM.STM (IO [MsgOut])
remUserSTM us user = do
  users <- STM.readTVar us
  case remUser user users of
    Right users' -> do
      STM.writeTVar us users'
      pure $ T.putStrLn ("User '" <> _uName user <> "' removed.\n  " <> T.pack (show $ M.keys users')) >> pure []
    Left err -> do
      pure $ T.putStrLn ("Error when removing new user: " <> printErr err) >> pure []

closeSocksSTM :: STM.TVar Users -> User -> STM.STM (IO [MsgOut])
closeSocksSTM us user = do
  users <- STM.readTVar us
  case closeSocks user users of
    Right (users', _) -> do
      STM.writeTVar us users'
      pure $ T.putStrLn ("User socks closed.\n  " <> _uName user) >> pure []
    Left err -> do
      pure $ T.putStrLn ("Error when add new user: " <> printErr err) >> pure []

openSocksSTM :: STM.TVar Users -> User -> Socks -> STM.STM (IO [MsgOut])
openSocksSTM us user socks = do
  users <- STM.readTVar us
  case openSocks socks user users of
    (users', _) -> do
      STM.writeTVar us users'
      pure $ T.putStrLn ("User socks opened.\n  " <> _uName user) >> pure []


{- | outHandler

resposible on getting messages from users and relay them to the rest of the Router
-}

outHandler :: STM.TQueue MsgOut -> STM.TQueue MsgOut -> IO ()
outHandler input output = STM.atomically $ do
  STM.writeTQueue output =<< STM.readTQueue input
