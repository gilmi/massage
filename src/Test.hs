module Test where

import Control.Monad (forever, void, (<=<))

import qualified Control.Concurrent as C
import qualified Control.Monad.STM as STM
import qualified Control.Concurrent.STM as STM

import Handler
import Users as Users
import Rooms as Rooms


openUsers :: IO UsersHandler
openUsers =
  uncurry Users.new =<< newOH mempty

openRooms :: IO RoomsHandler
openRooms =
  uncurry Rooms.new =<< newOH mempty


newOH :: Show a => b -> IO (STM.TVar b, STM.TQueue a)
newOH v = do
  out <- STM.newTQueueIO
  var <- STM.newTVarIO v
  void $ C.forkIO $ forever (oh out >> C.yield)
  pure (var, out)

  where
    oh = print <=< STM.atomically . STM.readTQueue


send :: Handler i -> i -> IO ()
send h m = STM.atomically $ STM.writeTQueue (_uhIn h) m
