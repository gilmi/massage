{- | Handlers

     An abstraction for OO components used for communication

-}

{-# LANGUAGE TemplateHaskell #-}

module Handler where

import Control.Monad (void, forever)
import qualified Control.Concurrent as C
import qualified Control.Monad.STM as STM
import qualified Control.Concurrent.STM as STM

import Data.Label

data Handler msgIn = Handler
  { _uhIn  :: STM.TQueue msgIn
  , _uhTid :: (C.ThreadId, C.ThreadId)
  }

mkLabel ''Handler

type InHandler state msgIn msgOut = STM.TVar state -> STM.TQueue msgIn -> IO [msgOut]

type OutHandler msgOut
  =  STM.TQueue msgOut -- ^ Input from InHandler
  -> STM.TQueue msgOut -- ^ Output
  -> IO () -- ^ Actions


newHandler
  :: STM.TVar state
  -> InHandler state msgIn msgOut
  -> OutHandler msgOut
  -> STM.TQueue msgOut
  -> IO (Handler msgIn)
  
newHandler statevar ih oh routerPort = do
  (qIn, qOut) <- STM.atomically $
    (,)
      <$> STM.newTQueue
      <*> STM.newTQueue
  tIdIn  <- C.forkIO $ forever (knotHandler ih qOut statevar qIn >> C.yield)
  tIdOut <- C.forkIO $ forever (oh routerPort qOut >> C.yield)
  pure (Handler qIn (tIdIn, tIdOut))


knotHandler :: InHandler s i o -> STM.TQueue o -> InHandler s i o
knotHandler ih oq var queue = do
  msgs <- ih var queue
  void $ STM.atomically $
    traverse (STM.writeTQueue oq) msgs
  pure msgs
