{-# LANGUAGE TemplateHaskell #-}

-- | Utils

module Utils where

import qualified Network.Socket as Net (Socket)
import qualified Data.Sequence as Seq
import Data.Label (mkLabels)

type Queue a = Seq.Seq a

push :: a -> Queue a -> Queue a
push x xs = xs Seq.|> x

pop :: Queue a -> (Maybe a, Queue a)
pop q = (Seq.lookup 0 q', q'')
  where
    (q', q'') = Seq.splitAt 0 q

emptyQueue :: Queue a
emptyQueue = Seq.fromList []

throwErr :: e -> Either e a
throwErr = Left

-- | Socks

-- | connection in and out
type Socks = Net.Socket

-- | connection in and out
data Conn a = Conn
  { _uQueueIn  :: Queue a
  , _uQueueOut :: Queue a
  , _conn :: Maybe Socks
  }

mkLabels [''Conn]

pushback :: a -> [a] -> [a]
pushback x xs = xs ++ [x]

defaultConn :: Conn a
defaultConn = Conn Seq.empty Seq.empty Nothing



