{-# LANGUAGE TemplateHaskell, TypeOperators, OverloadedStrings #-}

{- | User communication

This module handles the communication with a user

-}

module User where

import Control.Concurrent (forkIO, yield)
import qualified Network.Socket.ByteString as Net

import Control.Monad (void, guard, forever)
import Control.Arrow (first)
import Control.Applicative

import qualified Data.ByteString.Char8 as BS

import qualified Control.Monad.IO.Class as MT

import qualified Control.Monad.STM as STM
import qualified Control.Concurrent.STM as STM


import Types
import Utils
import qualified Handler as H

import Data.Label as Lens

{- | Design


-}

data MsgIn
  = SendMessage Message
  | OpenSocks Socks
  | UserMessage BS.ByteString

data MsgOut
  = GotMessage BS.ByteString
  deriving Show

data UserState = UserState
  { _uSocks :: Maybe Socks
  , _uBacklog :: Queue Message
  }
  deriving (Show)

emptyUserState :: UserState
emptyUserState = UserState Nothing emptyQueue

mkLabel ''UserState

-- | Remember socks
openSocks :: Socks -> UserState -> UserState
openSocks s u = u { _uSocks = Just s }

-- | Forget socks
closeSocks :: UserState -> UserState
closeSocks u = u { _uSocks = Nothing }

sendMessage :: Message -> UserState -> IO (Bool, UserState)
sendMessage m (Lens.modify uBacklog (push m) -> u) = sendBacklogMessage u

mep :: Alternative f => Maybe a -> f a
mep = maybe empty pure

sendBacklogMessage :: UserState -> IO (Bool, UserState)
sendBacklogMessage u = (<|> pure (False, u)) $ fmap (False,) $ do
  socks <-  mep $ Lens.get uSocks u
  let (mayMsg, rest) = first (fmap (BS.pack . show)) $ pop $ Lens.get uBacklog u
  msg <- mep mayMsg
  len <- MT.liftIO $ Net.send socks msg
  guard (len == BS.length msg)
  pure $ Lens.modify uBacklog (const rest) u

type UserHandler = H.Handler MsgIn

new :: STM.TVar UserState -> STM.TQueue MsgOut -> IO UserHandler
new statevar out = do
  h <- H.newHandler statevar inHandler outHandler out
  void $ forkIO $ receiver statevar (Lens.get H.uhIn h)
  pure h

receiver :: STM.TVar UserState -> STM.TQueue MsgIn -> IO ()
receiver statevar input = forever . (*> yield) $ do
  undefined

{- | inHandler

resposible on getting messages from inside the application and handle them
-}

inHandler :: STM.TVar UserState -> STM.TQueue MsgIn -> IO [MsgOut]
inHandler us input = do
  (user, action) <- STM.atomically $
    (,)
      <$> STM.readTVar us
      <*> STM.readTQueue input
  case action of
    UserMessage msg ->
      pure [GotMessage msg]

    SendMessage msg -> do
      (_, us') <- sendMessage msg user
      STM.atomically $ STM.writeTVar us us'
      pure []

    OpenSocks socks -> do
      STM.atomically $ STM.writeTVar us (openSocks socks user)
      pure []

{- | outHandler

resposible on getting messages from users and relay them to the rest of the Router
-}

outHandler :: STM.TQueue MsgOut -> STM.TQueue MsgOut -> IO ()
outHandler input output = STM.atomically $ do
  STM.writeTQueue output =<< STM.readTQueue input
