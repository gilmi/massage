{-# LANGUAGE TemplateHaskell, TypeOperators, OverloadedStrings #-}

{- | Router

This component is the router of communcation in the application.
It spawns the Users and Rooms components and oversees the communication
between them.

It's responsible for creating sockets for new users and sign them to the system.

-}

module Router where


import qualified Control.Monad.STM as STM
import qualified Control.Concurrent.STM as STM
import qualified Network.Socket as Net

import Types
import Utils
import qualified Handler as H
import qualified Users
import qualified Rooms

{- | Design

-}

data State = State
  { rRooms :: Rooms.RoomsHandler
  , rUsers :: Users.UsersHandler
  }

data MsgIn
  = UsersIn Users.MsgOut
  | RoomsIn Rooms.MsgOut

data MsgOut
  = UsersOut Users.MsgIn
  | RoomsOut Rooms.MsgIn

new :: State -> STM.TQueue MsgOut -> IO RouterHandler
new state = H.newHandler (\(_ :: STM.TVar ()) -> inHandler) (\input _ -> outHandler state input)

type RouterHandler = H.Handler MsgIn

inHandler :: STM.TQueue MsgIn -> IO [MsgOut]
inHandler input = do
  action <- STM.atomically $ do
    action <- STM.readTQueue input
    case action of
      UsersIn (Users.GotMessage msg) -> do
        pure $ pure [RoomsOut $ Rooms.SendMessage msg]
        
      UsersIn _ -> pure $ pure []
      RoomsIn _ -> pure $ pure []

  action


outHandler :: State -> STM.TQueue MsgOut -> IO ()
outHandler state queue = STM.atomically $
  STM.readTQueue queue >>= \case
    RoomsOut msg -> do
      STM.writeTQueue (H._uhIn $ rRooms state) msg
    UsersOut msg -> do
      STM.writeTQueue (H._uhIn $ rUsers state) msg
